"""Top-level package for goldenrecorder."""

__author__ = """Seburath"""
__email__ = 'seburath@gmail.com'
__version__ = '0.1.0'


from .ipvideorecorder import IPVideoRecorder
