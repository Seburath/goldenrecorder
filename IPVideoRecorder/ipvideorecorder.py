"""Main module."""
import cv2
from time import sleep


class IPVideoRecorder:
    def __init__(self, cam_address, directory):
        """
        :param cam_address: ip address of the camera feed
        :param directory: folder were the video will be saved
        """

        self.cam_address = cam_address
        self.directory = directory
        self.capturer = None

        self.RECONNECTION_PERIOD = 1

        self.connect()

    def connect(self):
        while True:
            try:
                self.capturer = cv2.VideoCapture(self.cam_address)

                if not self.capturer.isOpened():
                    raise Exception(f"Could not connect to: {self.cam_address}")

                print(f"Connected to: {self.cam_address}")
                break

            except Exception as e:
                print(e)
                sleep(self.RECONNECTION_PERIOD)

    def read(self):
        """
        Reads frame and if frame is not received tries to reconnect the camera

        :return: ret - bool witch specifies if frame was read successfully
                 frame - opencv image from the camera
        """

        ret, frame = self.capturer.read()

        if ret is False:
            self.connect()

        return ret, frame

    def write(self, frame):
        codec = cv2.VideoWriter_fourcc(*'XVID')
        framesize = (int(self.capturer.get(3)), int(self.capturer.get(4)))
        name = "video.avi"

        self.writer = cv2.VideoWriter(self.directory + name, codec, 20.0, framesize)
        self.writer.write(frame)

    def start(self):
        print("IPVideoRecorder started, press q to stop it gracefully.")

        while True:
            frame_captured, frame = self.read()

            if frame_captured:
                self.write(frame)
                cv2.imshow(CAM_ADDRESS, frame)

            if cv2.waitKey(1) == ord("q"):
                break

        self.capturer.release()
        self.writer.release()
        cv2.destroyAllWindows()

        print("IPVideoRecorder stoped.")
