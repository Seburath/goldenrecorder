#!/usr/bin/env python3

import yaml
from .ipvideorecorder import IPVideoRecorder


with open('config.yml', "r") as file:
    config = dict(yaml.safe_load(file))

IPVR = IPVideoRecorder(config["CAM_ADDRESS"], config["DIRECTORY"])
IPVR.start()
