==============
goldenrecorder
==============


.. image:: https://img.shields.io/pypi/v/goldenrecorder.svg
        :target: https://pypi.python.org/pypi/goldenrecorder

.. image:: https://img.shields.io/travis/Seburath/goldenrecorder.svg
        :target: https://travis-ci.com/Seburath/goldenrecorder

.. image:: https://readthedocs.org/projects/goldenrecorder/badge/?version=latest
        :target: https://goldenrecorder.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status




ip cam video recorder


* Free software: MIT license
* Documentation: https://goldenrecorder.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
